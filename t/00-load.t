#!perl

use Test::More tests => 1;

BEGIN {
    use_ok( 'Packaging::Tools' ) || print "Bail out!\n";
}

diag( "Testing Packaging::Tools $Packaging::Tools::VERSION, Perl $], $^X" );

#!/usr/bin/perl

use strict;
use warnings;

require Packaging::Tools;

require File::Basename;
require File::Spec;

use Getopt::Long;
use Pod::Usage;
use Template;

my %tools = (
    #@toolhash@
    make         => '/usr/pkg/bin/bmake',
    pkg_info_cmd => '/usr/pkg/sbin/pkg_info',
    pkgsrcdir    => '/data/pkgsrc',
);

#my $pkgdist = '@pkgdist@';
my $pkgdist = 'pkgsrc';

my @pkgs_in_state;
my $pkgtool = Packaging::Tools->new( $pkgdist, \%tools );

my @templates   = $pkgtool->find_templates("up2date");
my %tpl_names   = map { scalar File::Basename::fileparse( $_, qr/\.[^.]*/ ) => $_ } @templates;
my %opts        = ( "update-cpan-index" => 0 );
my @fmt_options = map { "output-" . $_ } keys %tpl_names;
my @options =
  ( ( map { $_ . ":s" } @fmt_options ), "cpan-home=s", "update-cpan-index!", "help|h", "usage|?" );

GetOptions( \%opts, @options ) or pod2usage(2);

defined( $opts{help} )
  and $opts{help}
  and pod2usage(
                 {
                   -verbose => 2,
                   -exitval => 0
                 }
               );
defined( $opts{usage} ) and $opts{usage} and pod2usage(1);

my @output = grep { defined( $opts{$_} ) } @fmt_options;
@output or @output = ('output-log');

$pkgtool->load_cpan_config( $opts{"cpan-home"} );
$pkgtool->get_cpan_versions( $opts{"update-cpan-index"} );

my ( $up_to_date, $need_update, $need_check ) = (0) x 3;
my @pkglist = $pkgtool->find_packaged_modules();
@pkglist = sort @pkglist;
foreach my $pkg (@pkglist)
{
    my $state = $pkgtool->check_pkg_up2date_state($pkg);
    if ($state)
    {
        my %pkg_details;
        my @pkg_detail_keys =
          qw(DIST_NAME DIST_VERSION DIST_FILE PKG_NAME PKG_VERSION PKG_MAINTAINER PKG_HOMEPAGE PKG_INSTALLED PKG_LOCATION CPAN_VERSION CPAN_NAME CHECK_STATE CHECK_COMMENT);
        @pkg_details{@pkg_detail_keys} = $pkgtool->get_pkg_details( $pkg, @pkg_detail_keys );
        push( @pkgs_in_state, \%pkg_details );
        if ( $pkg_details{CHECK_STATE} == Packaging::Tools->STATE_NEWER_IN_CPAN )
        {
            ++$need_update;
        }
        else
        {
            ++$need_check;
        }
    }
    else
    {
        ++$up_to_date;
    }
}

my $template = Template->new(
                              INCLUDE_PATH => join( ":",
                                                    $pkgtool->get_template_directories("up2date"),
                                                    $pkgtool->get_template_directories() ),
                              ABSOLUTE => 1,
                            );
my %vars = (
             data          => \@pkgs_in_state,
             STATE_REMARKS => [ $pkgtool->get_state_remarks() ],
             STATE_CMPOPS  => [ $pkgtool->get_state_cmpops() ],
             COUNT         => {
                        UP2DATE     => $up_to_date,
                        ENTIRE      => scalar(@pkglist),
                        NEED_UPDATE => $need_update,
                        NEED_CHECK  => $need_check
                      },
           );
foreach my $tgt (@output)
{
    my $tgtfn = $opts{$tgt};
    ( my $tpl = $tgt ) =~ s/output-(.*)/$1/;
    unless ($tgtfn)
    {
        # for backward compatibility
        ( my $ext = $tgt ) =~ s/output-([^-]+).*/$1/;
        $tgtfn = File::Spec->catfile( $ENV{HOME}, "up2date." . $ext );
    }
    $template->process( $tpl_names{$tpl}, \%vars, $tgtfn )
      or die $template->error();
}

exit(0);

#!/usr/bin/perl

use strict;
use warnings;

require Packaging::Tools;

require File::Basename;
require File::Spec;

use Getopt::Long;
use Pod::Usage;
use Template;

my %tools = (
    #@toolhash@
    MAKE         => '/usr/pkg/bin/bmake',
    PKG_INFO_CMD => '/usr/pkg/sbin/pkg_info',
    PKGSRCDIR    => '/data/pkgsrc',
);

#my $pkgdist = '@pkgdist@';
my $pkgdist = 'pkgsrc';

my @pkgs_in_state;
my $pkgtool = Packaging::Tools->new( $pkgdist, \%tools );

my @templates   = $pkgtool->find_templates("pkg4module");
my %tpl_names   = map { scalar File::Basename::fileparse( $_, qr/\.[^.]*/ ) => $_ } @templates;
my %opts        = ( "update-cpan-index" => 0 );
my @fmt_options = map { "output-" . $_ } keys %tpl_names;
my @options = (
                ( map { $_ . ":s" } @fmt_options ),
                "cpan-home=s", "update-cpan-index!", "module=s@", "help|h", "usage|?"
              );

GetOptions( \%opts, @options ) or pod2usage(2);

defined( $opts{help} )
  and $opts{help}
  and pod2usage(
                 {
                   -verbose => 2,
                   -exitval => 0
                 }
               );
defined( $opts{usage} ) and $opts{usage} and pod2usage(1);
defined( $opts{module} ) or pod2usage(1);

my @output = grep { defined( $opts{$_} ) } @fmt_options;
@output or @output = ('output-log');

$pkgtool->load_cpan_config( $opts{"cpan-home"} );
$pkgtool->get_cpan_versions( $opts{"update-cpan-index"} );

my ( $up_to_date, $need_update, $need_check ) = (0) x 3;
my %pkglist = map { $_ => 1 } $pkgtool->find_packaged_modules();

my @distris;
my @modules = split( ",", join( ",", @{ $opts{module} } ) );
foreach my $module (@modules)
{
    printf( "Searching for %s\n", $module );
    push( @distris, $pkgtool->get_distribution_for_module($module) );
}

use Data::Dumper;
print Dumper \@distris;
exit(0);

my $template = Template->new(
                              INCLUDE_PATH => join( ":",
                                                   $pkgtool->get_template_directories("pkg4module"),
                                                   $pkgtool->get_template_directories() ),
                              ABSOLUTE => 1,
                            );
my %vars = ( data => \@distris, );
foreach my $tgt (@output)
{
    my $tgtfn = $opts{$tgt};
    ( my $tpl = $tgt ) =~ s/output-(.*)/$1/;
    unless ($tgtfn)
    {
        # for backward compatibility
        ( my $ext = $tgt ) =~ s/output-([^-]+).*/$1/;
        $tgtfn = File::Spec->catfile( $ENV{HOME}, "pkg4module." . $ext );
    }
    $template->process( $tpl_names{$tpl}, \%vars, $tgtfn )
      or die die $template->error();
}

exit(0);

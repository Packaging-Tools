package Packaging::Tools::Plugin;

use 5.014;

use strict;
use warnings;

=head1 NAME

Packaging::Tools::Plugin - base class for Packaging::Tools plugins to define API

=cut

use Carp qw/carp croak/;

our $VERSION = '0.001';

require File::Basename;
require File::Spec;

use Moo;
use namespace::clean;

has '_column_defs' => (
                        is       => 'ro',
                        builder  => '_default_column_defs',
                        init_arg => undef
                      );
has 'packaged_modules' => (
                            is  => 'ro',
                            isa => sub { "HASH" eq ref( $_[0] ) or die "$_[0] is not a hashref!"; },
                            lazy     => 1,
                            builder  => 'find_packaged_modules',
                            init_arg => undef
                          );
has 'installed_packages' => (
                            is  => 'ro',
                            isa => sub { "HASH" eq ref( $_[0] ) or die "$_[0] is not a hashref!"; },
                            lazy     => 1,
                            builder  => 'find_installed_packages',
                            init_arg => undef
);

eval { require Packaging::Tools::Plugin::Role::Cache; }
  and with "Packaging::Tools::Plugin::Role::Cache";

=head1 SYNOPSIS

=head1 SUBROUTINES/METHODS

=cut

sub _default_column_defs
{
    return [
             qw(PKG_IDENT DIST_NAME DIST_VERSION DIST_FILE),
             qw(PKG_NAME PKG_VERSION PKG_MAINTAINER PKG_INSTALLED),
             qw(PKG_LOCATION PKG_HOMEPAGE PKG_LICENSE PKG_MASTER_SITES)
           ];
}

=head2 find_installed_packages

=cut

sub find_installed_packages { ... }

sub _find_packaged_modules { ... }

=head2 find_packaged_modules

=cut

sub find_packaged_modules
{
    my ($self) = @_;

    return $self->_find_packaged_modules();
}

sub _get_pkg_details { ... }

=head2 get_pkg_details($pkg_ident,@var_names)

Returns package details.

=over 4

=item * I<$pkg_ident>

One value from the list of L</find_packaged_modules|packaged modules>.

=item * I<@var_names>

Any combination of:

=over 8

=item DIST_NAME

=item DIST_VERSION

=item DIST_FILE

=item PKG_NAME

=item PKG_VERSION

=item PKG_MAINTAINER

=item PKG_LOCATION

=item PKG_LICENSE

=item PKG_HOMEPAGE

=item PKG_INSTALLED

=item PKG_MASTER_SITES

=back

=back

=cut

sub get_pkg_details
{
    my ( $self, $pkg_ident, @var_names ) = @_;

    defined( $self->packaged_modules()->{$pkg_ident} )
      or $self->_get_pkg_details( $pkg_ident, @var_names );

    return @{ $self->packaged_modules()->{$pkg_ident} }{@var_names};
}

sub find_package
{
    my ( $self, %match ) = @_;

    scalar( keys %match ) > 0 or croak("find_package( key => value )");

    my @keys   = keys %match;
    my @values = @match{@keys};
    my $pkgs   = $self->packaged_modules;
    my @found  = grep { @values ~~ @{ $pkgs->{$_} }{@keys}; } keys %{$pkgs};

    1 == scalar(@found) and return $found[0];
    return;
}

has 'template_directories' => (
                                is       => 'ro',
                                lazy     => 1,
                                init_arg => undef,
                                builder  => '_get_template_directories'
                              );

sub _get_template_directories { return (); }

no Moo;

1;

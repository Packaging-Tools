package Packaging::Tools::Plugin::Role::Cache;

use 5.014;

use strict;
use warnings;

=head1 NAME

Packaging::Tools::Plugin::Role::Cache - role allows caching expensive data of Packaging::Tools::Plugin

=cut

use Carp qw/carp croak/;

our $VERSION = '0.001';

require File::Basename;
require File::Spec;

use DBI;
require DBD::SQLite;
use File::ConfigDir qw(config_dirs);    # File::DataDir
require File::Find::Rule;
use File::Path qw(make_path);

use Moo::Role;
use namespace::clean;

has '_dbh' => (
                is       => 'rw',
                isa      => 'DBI::db',
                isa      => sub { $_[0]->isa("DBI::db") or die "$_[0] is not a DBI::db!"; },
                clearer  => '_disco',
                builder  => '_open_cache',
                init_arg => undef
              );
has '_last_scan' => (
                      is       => 'rw',
                      builder  => '_fetch_last_scan',
                      trigger  => sub { $_[0]->_persist_last_scan( $_[1] ) },
                      init_arg => undef
                    );

requires '_column_defs';
requires '_find_packaged_modules';
requires 'find_packaged_modules';
requires 'find_package';

# not multi-process/thread safe ...
sub _create_cache
{
    my ( $self, $basedir ) = @_;
    my $db_name;

    ( my $cache_basename = ref($self) ) =~ s/.*::([^:]+)$/$1/;
    if ($basedir)
    {
        $db_name = File::Spec->catfile( $basedir, "$cache_basename.db" );
    }
    else
    {
        my @config_dirs = config_dirs("Packaging-Tools");    # File::DataDir
        if ( 0 == scalar(@config_dirs) )
        {
            @config_dirs = config_dirs();                    # File::DataDir
            foreach my $cfg_dir (@config_dirs)
            {
                -w $cfg_dir or next;
                my $pt_cfg_dir = File::Spec->catdir( $cfg_dir, "Packaging-Tools" );
                make_path( $pt_cfg_dir, { mode => 0755, } );
                $db_name = File::Spec->catfile( $pt_cfg_dir, "$cache_basename.db" );
                last;
            }
        }

        if ( !$db_name )
        {
            foreach my $cfg_dir (@config_dirs)
            {
                -w $cfg_dir or next;
                $db_name = File::Spec->catfile( $cfg_dir, "$cache_basename.db" );
                last;
            }
        }
    }

    my $dbh = DBI->connect( "dbi:SQLite:dbname=$db_name", "", "" );

    my $mkpkgtbl = <<EOT;
CREATE TABLE IF NOT EXISTS DIST_PKGS (
    PKG_IDENT VARCHAR(256) PRIMARY KEY,
    DIST_NAME VARCHAR(32),
    DIST_VERSION VARCHAR(16),
    DIST_FILE VARCHAR(64),
    PKG_NAME VARCHAR(32),
    PKG_VERSION VARCHAR(16),
    PKG_MAINTAINER VARCHAR(64),
    PKG_INSTALLED BOOLEAN,
    PKG_LOCATION VARCHAR(128),
    PKG_HOMEPAGE VARCHAR(256),
    PKG_LICENSE VARCHAR(64),
    PKG_MASTER_SITES TEXT
);
EOT

    my $sth = $dbh->prepare($mkpkgtbl);
    $sth->execute();
    $sth->finish();

    my $mklasttbl = <<EOT;
CREATE TABLE IF NOT EXISTS LAST_SCAN (
    LAST_SCAN BIGINT
);
EOT

    $sth = $dbh->prepare($mklasttbl);
    $sth->execute();
    $sth->finish();

    return $dbh;
}

sub _open_cache
{
    my ( $self, $basedir ) = @_;

    ( my $cache_basename = ref($self) ) =~ s/.*::([^:]+)$/$1/;

    my @search_dirs = $basedir ? ($basedir) : config_dirs("Packaging-Tools");    # File::DataDir
    scalar(@search_dirs) == 0 and return $self->_create_cache($basedir);
    my @cache_files =
      File::Find::Rule->name( $cache_basename . ".db" )->maxdepth(1)->in(@search_dirs);
    scalar(@cache_files) > 1
      and croak( "More than one cache file found: " . join( ", ", @cache_files ) );
    scalar(@cache_files) == 0 and return $self->_create_cache($basedir);

    my $dbh = DBI->connect( "dbi:SQLite:dbname=" . $cache_files[0], "", "" );

    return $dbh;
}

sub _update_cache
{
    my ( $self, %data ) = @_;

    my @colnames = @{ $self->_column_defs() };

    my $dbh     = $self->_dbh;
    my @coldata = @data{@colnames};
    my $sql     = sprintf( "INSERT OR IGNORE INTO DIST_PKGS (%s) VALUES (%s)",
                       join( ", ", @colnames ), join( ", ", map { $dbh->quote($_) } @coldata ) );
    my $sth;

    $sth = $dbh->prepare($sql);
    if ( !$sth->execute() )    # should be 1
    {
        my $pk = shift @colnames;
        $sql =
            "UPDATE OR IGNORE DIST_PKGS SET "
          . join( ", ", map { "$_=" . $dbh->quote( $data{$_} ) } @colnames )
          . " WHERE $pk="
          . $dbh->quote( $data{$pk} );
        $sth = $dbh->prepare($sql);
        $sth->execute() or croak( $dbh->errstr() );
    }

    return;
}

sub _fetch_last_scan
{
    my $self = shift;

    $self->_dbh or $self->_open_cache();

    my $dbh           = $self->_dbh;
    my $sql           = "SELECT LAST_SCAN FROM LAST_SCAN";
    my @last_scan_row = $dbh->selectrow_array($sql);
    @last_scan_row and return $last_scan_row[0];

    return 0;    # must be epoch
}

sub _persist_last_scan
{
    my ( $self, $new_last_scan ) = @_;
    my $dbh = $self->_dbh;
    my $sql = "delete from LAST_SCAN";
    $dbh->do($sql) or warn $dbh->errstr();
    $sql = "insert into LAST_SCAN (LAST_SCAN) values ($new_last_scan)";
    $dbh->do($sql) or warn $dbh->errstr();
    return;
}

around 'find_packaged_modules' => sub {
    my $orig = shift;
    my $self = shift;

    $self->_dbh or $self->_open_cache();

    my $dbh           = $self->_dbh;
    my $sql           = "SELECT LAST_SCAN FROM LAST_SCAN";
    my @last_scan_row = $dbh->selectrow_array($sql);

    my $last_scan = $self->_last_scan;
    $self->_last_scan( time() );
    my %pkg_idents = %{ $self->_find_packaged_modules($last_scan) };

    local $dbh->{FetchHashKeyName} = 'NAME_uc';
    $sql = "SELECT * FROM DIST_PKGS";
    my $sth         = $dbh->prepare($sql);
    my $rows        = $sth->execute() or croak( $dbh->errstr() );
    my $pkg_details = $sth->fetchall_hashref( $self->_column_defs()->[0] );
    delete @{$pkg_details}{ keys %pkg_idents };

    # update refreshed info ...
    foreach my $pkg_id ( keys %pkg_idents )
    {
        $self->_update_cache( PKG_IDENT => $pkg_id,
                              %{ $pkg_idents{$pkg_id} } );
    }

    %pkg_idents = ( %{$pkg_details}, %pkg_idents );

    return \%pkg_idents;
};

around 'find_package' => sub {
    my $orig  = shift;
    my $self  = shift;
    my %match = @_;

    $self->_dbh or $self->_open_cache();

    scalar( keys %match ) > 0 or croak("find_package( key => value )");

    my $dbh = $self->_dbh;
    my $sql = sprintf( "SELECT DISTINCT %s FROM DIST_PKGS WHERE %s",
                       $self->_column_defs()->[0],
                       join( " AND ", map { $_ . "=" . $dbh->quote( $match{$_} ) } keys %match ) );
    my @found = $dbh->selectrow_array($sql);
    1 == scalar(@found) and return $found[0];
    return;
};

around get_pkg_details => sub {
    my $orig = shift;
    my $self = shift;

    $self->_dbh or $self->_open_cache();

    my ( $pkg_ident, @var_names ) = @_;

    return @{ $self->packaged_modules()->{$pkg_ident} }{@var_names};
};

no Moo::Role;

1;

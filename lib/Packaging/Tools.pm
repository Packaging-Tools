package Packaging::Tools;

use 5.008;
use strict;
use warnings;
use version;

use Carp qw/croak/;

use CPAN;
use CPAN::DistnameInfo;
use Module::CoreList;

require File::Basename;
require File::ShareDir;
require File::Find::Rule;
require File::Spec;

use Moo;
use namespace::clean;

=head1 NAME

Packaging::Tools - Support tools packagers packaging Perl 5 modules

=cut

our $VERSION = '0.001';

use constant {
               STATE_OK                 => 0,
               STATE_NEWER_IN_CPAN      => 1,
               STATE_NEWER_IN_CORE      => 2,
               STATE_OUT_OF_SYNC        => 3,
               STATE_REMOVED_FROM_INDEX => 4,
               STATE_ERROR              => 101,
             };

my @state_remarks;
$state_remarks[STATE_OK]                 = 'fine';
$state_remarks[STATE_NEWER_IN_CPAN]      = 'needs update';
$state_remarks[STATE_NEWER_IN_CORE]      = 'newer in Core';
$state_remarks[STATE_OUT_OF_SYNC]        = 'out of sync';
$state_remarks[STATE_REMOVED_FROM_INDEX] = 'not in CPAN index';
$state_remarks[STATE_ERROR]              = '';

my @state_cmpops;
$state_cmpops[STATE_OK]            = '==';
$state_cmpops[STATE_NEWER_IN_CPAN] = '<';
$state_cmpops[STATE_OUT_OF_SYNC]   = '!=';

=head1 SYNOPSIS

=head1 SUBROUTINES/METHODS

=head2 new($dist_type,\%args)

Instantiates new Packaging::Tools instance and connects to a
Packaging::Tools::Plugin::$dist_type instance.

The plugin is installed with \%args as parameters.

=cut

sub new
{
    my ( $class, $dist_type, $args ) = @_;

    eval "require Packaging::Tools::Plugin::$dist_type" or die $@;

    my $dist_type_class = "Packaging::Tools::Plugin::$dist_type";

    my $self = bless( {}, $class );
    $self->{_dist_tools} = $dist_type_class->new(%$args);
    $self->{_pkg_dist}   = $dist_type;

    return $self;
}

=head2 get_state_remarks

Return the list of the remarks for the known states.

See the example templates how to use it.

=cut

sub get_state_remarks { return @state_remarks; }

=head2 get_state_cmpops

Return the list of compare operations for the known states (e.g. "<" for
STATE_NEWER_IN_CPAN).

See the example templates how to use it.

=cut

sub get_state_cmpops { return @state_cmpops; }

=head2 get_template_directories(;$tool)

Returns the directories containing the templates, either in general or for
specific tools.

=cut

sub get_template_directories
{
    my ( $self, $tool ) = @_;

    my @tt_src_dirs =
      grep { defined($_) and -d $_ }
      ( File::ShareDir::dist_dir("Packaging-Tools"), $self->{_dist_tools}->template_directories() );

    $tool and return map { File::Spec->catdir( $_, $tool ) } @tt_src_dirs;

    return @tt_src_dirs;
}

=head2 find_templates(;$tool)

Find templates (full qualified path name) for processing, either in general
or for specific tools.

=cut

sub find_templates
{
    my ( $self, $tool ) = @_;

    $tool //= File::Basename::fileparse( $0, qr/\.[^.]*/ );

    my @tt_src_dirs = $self->get_template_directories($tool);
    my @templates   = File::Find::Rule->file()->name("*.tt2")->maxdepth(1)->in(@tt_src_dirs);

    return @templates;
}

=head2 load_cpan_config(;$cpan_home)

Loads the CPAN config. If a I<$cpan_home> is given and the is a
C<CPAN/MyConfig.pm> in there, this file is loaded. If a I<$cpan_home>
is given without a C<CPAN/MyConfig.pm> in there, I<$CPAN::Config{cpan_home}>
is set to given I<$cpan_home>. Otherwise, only CPAN::HandleConfig->load()
is called.

=cut

sub load_cpan_config
{
    my ( $self, $cpan_home ) = @_;

    if ( defined($cpan_home) and -e File::Spec->catfile( $cpan_home, 'CPAN', 'MyConfig.pm' ) )
    {
        my $file = File::Spec->catfile( $cpan_home, 'CPAN', 'MyConfig.pm' );

        # XXX taken from App:Cpan::_load_config()
        $CPAN::Config = {};
        delete $INC{'CPAN/Config.pm'};

        my $rc = eval { require $file };
        my $err_myconfig = $@;
        if ( $err_myconfig and $err_myconfig !~ m#locate \Q$file\E# )
        {
            croak "Error while requiring ${file}:\n$err_myconfig";
        }
        elsif ($err_myconfig)
        {
            CPAN::HandleConfig->load();
            defined( $INC{"CPAN/MyConfig.pm"} )
              and $CPAN::Config_loaded++;
            defined( $INC{"CPAN/Config.pm"} )
              and $CPAN::Config_loaded++;
        }
        else
        {
            # CPAN::HandleConfig::require_myconfig_or_config looks for this
            $INC{'CPAN/MyConfig.pm'} = 'fake out!';

            # CPAN::HandleConfig::load looks for this
            $CPAN::Config_loaded = 'fake out';
        }
    }
    else
    {
        CPAN::HandleConfig->load();
        defined( $INC{"CPAN/MyConfig.pm"} )
          and $CPAN::Config_loaded++;
        defined( $INC{"CPAN/Config.pm"} )
          and $CPAN::Config_loaded++;
        defined($cpan_home)
          and -d $cpan_home
          and $CPAN::Config{cpan_home} = $cpan_home;
    }

    $CPAN::Config_loaded
      or croak("Can't load CPAN::Config - please setup CPAN first");
}

=head2 get_cpan_versions(;$update_index)

(Re-)Loads the CPAN Index. When I<$update_index> is given and true, a newer
index is tried to fetch from configured mirror sites.

=cut

sub get_cpan_versions
{
    my ( $self, $update_idx ) = @_;

    defined( $self->{cpan_idx} ) and !$update_idx and return %{ $self->{cpan_idx} };
    $CPAN::Config_loaded or $self->load_cpan_config();

    $self->{cpan_idx} = {};

    defined($update_idx)
      and $update_idx
      and $CPAN::Index::LAST_TIME = 0;
    CPAN::Index->reload( defined($update_idx) and $update_idx );
    $CPAN::Index::LAST_TIME
      or carp("Can't reload CPAN Index");

    my @all_dists = $CPAN::META->all_objects("CPAN::Distribution");

    foreach my $dist (@all_dists)
    {
        my $dinfo = CPAN::DistnameInfo->new( $dist->id() );
        my ( $distname, $distver ) = ( $dinfo->dist(), $dinfo->version() );
        defined($distname) or next;
        defined($distver)  or next;
        if (
             !defined( $self->{cpan_idx}->{$distname} )
             || ( defined( $self->{cpan_idx}->{$distname} )
                  && _is_gt( $distver, $self->{cpan_idx}->{$distname} ) )
           )
        {
            $self->{cpan_idx}->{$distname} = $distver;
        }
    }

    return;
}

=head2 get_modules_by_distribution

Builds internal data structure of cpan available distributions
from index of modules.

=cut

sub get_modules_by_distribution
{
    my $self = shift;

    defined( $self->{cpan_mods_by_dist} ) and return;

    $self->get_cpan_versions();

    my @all_modules = $CPAN::META->all_objects("CPAN::Module");
    my %modsbydist;

    foreach my $module (@all_modules)
    {
        my $modname = $module->id();
        $module->cpan_version() or next;
        my $distfile = $module->cpan_file();
        my $dinfo    = CPAN::DistnameInfo->new($distfile);
        my ( $distname, $distver ) = ( $dinfo->dist(), $dinfo->version() );
        defined($distname) or next;
        defined($distver)  or next;
        $modsbydist{$distname} //= [];
        push( @{ $modsbydist{$distname} }, $modname );
    }

    $self->{cpan_mods_by_dist} = \%modsbydist;

    return;
}

=head2 get_installed_packages

Returns hash of name =E<gt> version of installed packages.

=cut

sub get_installed_packages
{
    my $self = $_[0];

    defined( $self->{installed_packages} )
      and return %{ $self->{installed_packages} };
    $self->{installed_packages} = { %{ $self->{_dist_tools}->installed_packages() } };

    return %{ $self->{installed_packages} };
}

=head2 find_packaged_modules

Returns list of packaged perl modules. The list has no specific order.

=cut

sub find_packaged_modules
{
    my $self = $_[0];

    $self->{packaged_modules} //= $self->{_dist_tools}->find_packaged_modules();    # XXX clone ...

    return keys %{ $self->{packaged_modules} };
}

sub _is_gt
{
    my $gt;
    defined( $_[0] ) and $_[0] =~ /^v/ and $_[1] !~ /^v/ and $_[1] = "v$_[1]";
    defined( $_[0] ) and $_[0] !~ /^v/ and $_[1] =~ /^v/ and $_[0] = "v$_[0]";
    eval { $gt = defined( $_[0] ) && ( version->parse( $_[0] ) > version->parse( $_[1] ) ); };
    if ($@)
    {
        $gt = defined( $_[0] ) && ( $_[0] gt $_[1] );
    }
    return $gt;
}

sub _is_ne
{
    my $ne;
    defined( $_[0] ) and $_[0] =~ /^v/ and $_[1] !~ /^v/ and $_[1] = "v$_[1]";
    defined( $_[0] ) and $_[0] !~ /^v/ and $_[1] =~ /^v/ and $_[0] = "v$_[0]";
    eval { $ne = defined( $_[0] )
          && ( version->parse( $_[0] ) != version->parse( $_[1] ) ); };
    if ($@)
    {
        $ne = defined( $_[0] ) && ( $_[0] ne $_[1] );
    }
    return $ne;
}

=head2 get_pkg_details($pkg_ident,@var_names)

Returns package details.

=over 4

=item * I<$pkg_ident>

One value from the list of L</find_packaged_modules|packaged modules>.

=item * I<@var_names>

One from the variables the distribution tool plugin
L<Packaging::Tools::Plugin/get_pkg_details($pkg_ident,@var_names)|provides>
or any combination of:

=over 8

=item CPAN_VERSION

=item CPAN_NAME

=item CHECK_STATE

=item CHECK_COMMENT

=back

=back

=cut

sub get_pkg_details
{
    my ( $self, $pkg_ident, @var_names ) = @_;

    my @local_vars = grep { $_ =~ m/^(?:CPAN|CHECK)_/ } @var_names;
    my @pkg_vars = grep { !( $_ ~~ @local_vars ) } @var_names;

    my %result;
    @result{@pkg_vars} = $self->{_dist_tools}->get_pkg_details( $pkg_ident, @pkg_vars );

    unless ( defined( $self->{pkg_details}->{$pkg_ident} ) )
    {
        my $dist_name = $result{DIST_NAME}
          // $self->{_dist_tools}->get_pkg_details( $pkg_ident, 'DIST_NAME' );
        $dist_name or return;
        $self->{pkg_details}->{$pkg_ident}->{CPAN_VERSION} = $self->{cpan_idx}->{$dist_name};
        $self->{pkg_details}->{$pkg_ident}->{CPAN_NAME}    = $dist_name;
    }

    @result{@local_vars} = @{ $self->{pkg_details}->{$pkg_ident} }{@local_vars};

    return @result{@var_names};
}

=head2 check_pkg_up2date_state($pkg_ident)

Returns the state of specified package.

Returned state is one of

=over 4

=item STATE_OK

Distribution package is up to date compared against fetched CPAN Index.

=item STATE_NEWER_IN_CPAN

There is a newer version of packaged distribution in CPAN.

=item STATE_NEWER_IN_CORE

There is a newer version of at least one module provided by the most recent
package in the current used Perl Core.

=item STATE_REMOVED_FROM_INDEX

Cannot find distribution in CPAN Index, most likely the module was removed
or never indexed for any reason.

=item STATE_OUT_OF_SYNC

The distribution packaged version is newer than newest one found in fetched
CPAN Index, which can have either the same reason as for
STATE_REMOVED_FROM_INDEX or the distribution is provided from somewhere else
instead of CPAN.

=item STATE_ERROR

Error getting distribution information for specified package.

=back

=cut

sub check_pkg_up2date_state
{
    my ( $self, $pkg_ident ) = @_;

    defined( $self->{installed_packages} ) or $self->get_installed_packages();
    defined( $self->{cpan_mods_by_dist} )  or $self->get_modules_by_distribution();

    my @pkg_det_keys = (qw(DIST_NAME DIST_VERSION PKG_VERSION CPAN_VERSION MASTER_SITES));
    my ( $dist_name, $dist_version, $pkg_version, $cpan_version, $master_sites ) =
      $self->get_pkg_details( $pkg_ident, @pkg_det_keys );

    unless ( defined($dist_name) and defined($dist_version) )
    {
        $self->{pkg_details}->{$pkg_ident}->{CHECK_COMMENT} = 'Error getting distribution data';
        return $self->{pkg_details}->{$pkg_ident}->{CHECK_STATE} = STATE_ERROR;
    }

    $dist_name eq 'perl'
      and return $self->{pkg_details}->{$pkg_ident}->{CHECK_STATE} = STATE_OK;

    foreach my $distmod ( @{ $self->{cpan_mods_by_dist}->{$dist_name} } )
    {
        defined( $Module::CoreList::version{$]}->{$distmod} ) or next;
        my $mod = $CPAN::META->instance( "CPAN::Module", $distmod );
        if ( _is_gt( $Module::CoreList::version{$]}->{$distmod}, $mod->cpan_version() ) )
        {
            $self->{pkg_details}->{$pkg_ident}->{CORE_NEWER}->{$distmod} =
              [ $Module::CoreList::version{$]}->{$distmod}, $mod->cpan_version() ];
        }
    }
    if ( defined( $self->{pkg_details}->{$pkg_ident}->{CORE_NEWER} ) )
    {
        $self->{pkg_details}->{$pkg_ident}->{CHECK_COMMENT} =
          "$dist_name-$dist_version has newer modules in core: " . join(
            ", ",
            map {
                    $_ . " "
                  . $self->{pkg_details}->{$pkg_ident}->{CORE_NEWER}->{$_}->[0] . " > "
                  . $self->{pkg_details}->{$pkg_ident}->{CORE_NEWER}->{$_}->[1]
              } keys %{ $self->{pkg_details}->{$pkg_ident}->{CORE_NEWER} }
          );
        return $self->{pkg_details}->{$pkg_ident}->{CHECK_STATE} = STATE_NEWER_IN_CORE;
    }

    if ( !defined($cpan_version) )
    {
        defined($master_sites)
          and $master_sites !~ m/cpan/i
          and return $self->{pkg_details}->{$pkg_ident}->{CHECK_STATE} = STATE_OK;
        return $self->{pkg_details}->{$pkg_ident}->{CHECK_STATE} = STATE_REMOVED_FROM_INDEX;
    }
    elsif ( _is_gt( $cpan_version, $dist_version ) )
    {
        return $self->{pkg_details}->{$pkg_ident}->{CHECK_STATE} = STATE_NEWER_IN_CPAN;
    }
    elsif ( _is_ne( $cpan_version, $dist_version ) )
    {
        return $self->{pkg_details}->{$pkg_ident}->{CHECK_STATE} = STATE_OUT_OF_SYNC;
    }

    return $self->{pkg_details}->{$pkg_ident}->{CHECK_STATE} = STATE_OK;
}

sub get_distribution_for_module
{
    my ( $self, $module ) = @_;
    my @found;

    if ( $CPAN::META->exists( "CPAN::Module", $module ) )
    {
        my $cpan_mod  = $CPAN::META->instance( "CPAN::Module", $module );
        my $cpan_dist = CPAN::DistnameInfo->new( $cpan_mod->cpan_file() )->dist();
        my $pkg_ident = $self->{_dist_tools}->find_package( DIST_NAME => $cpan_dist );

        push @found, $pkg_ident;
    }

    defined( $Module::CoreList::version{$]}->{$module} ) and push @found, "Core ($])";

    return wantarray ? @found : join( ",", @found );
}

=head1 AUTHOR

Jens Rehsack, C<< <rehsack at cpan.org> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-packaging-tools at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=Packaging-Tools>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Packaging::Tools


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=Packaging-Tools>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/Packaging-Tools>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/Packaging-Tools>

=item * Search CPAN

L<http://search.cpan.org/dist/Packaging-Tools/>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2012 Jens Rehsack.

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See http://dev.perl.org/licenses/ for more information.


=cut

1;    # End of Packaging::Tools
